const SERVER_ERROR = "An error occurred while contacting server.";

const sendToNumeralService = async (numeral, clientId) => {
  try {
    const fetchNumeralServiceResponse = await fetch(
      `http://localhost:3001/numeral/convertToRoman?clientId=${clientId}`,
      {
        method: "POST",
        mode: "cors",
        headers: {
          "content-type": "application/json",
        },
        body: JSON.stringify({ numeral: `${numeral}` }),
      }
    );
    return fetchNumeralServiceResponse.text();
  } catch (error) {
    console.error(error);
    throw new Error(SERVER_ERROR);
  }
};

export { sendToNumeralService };
