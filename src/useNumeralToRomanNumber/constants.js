export const MINIMUM_ALLOWED_NUMERAL = 1;
export const MAXIMUM_ALLOWED_NUMERAL = 3999; // https://blog.prepscholar.com/roman-numerals-converter
