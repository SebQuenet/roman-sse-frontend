import { useState, useEffect, useCallback } from "react";
import validateAndFormatNumeral from "./validateAndFormatNumeral";

import { sendToNumeralService } from "../services/numeralService";
import { v4 as uuidv4 } from "uuid";

const useNumeralsToRomanNumbers = () => {
  const [numeral, setNumeral] = useState("");
  const [romanNumber, setRomanNumber] = useState("");
  const [error, setError] = useState("");
  const [isError, setIsError] = useState(false);
  const [isSending, setIsSending] = useState(false);
  const [clientId, setClientId] = useState(null);

  const [listening, setListening] = useState(false);

  useEffect(() => {
    if (!listening) {
      const clientId = uuidv4();
      setClientId(clientId);
      const events = new EventSource(
        `http://localhost:3001/events?clientId=${clientId}`
      );

      events.onmessage = (event) => {
        const parsedData = JSON.parse(event.data);
        setRomanNumber(parsedData);
      };

      setListening(true);
    }
  }, [listening, romanNumber]);

  const handleSendButtonClick = useCallback(async () => {
    if (isSending) return;
    setIsSending(true);
    try {
      await sendToNumeralService(numeral, clientId);
    } catch (error) {
      setRomanNumber("");
      setError(error.message);
      setIsError(true);
    } finally {
      setIsSending(false);
    }
  }, [numeral, isSending, clientId]);

  useEffect(() => {
    if (!numeral) {
      setError("");
      setIsError(false);
      return;
    }

    try {
      validateAndFormatNumeral(numeral);
      setError("");
      setIsError(false);
      return;
    } catch (error) {
      setError(error.message);
      setIsError(true);
      return;
    }
  }, [numeral]);

  return {
    clientId,
    numeral,
    setNumeral,
    romanNumber,
    error,
    isError,
    handleSendButtonClick,
  };
};

export default useNumeralsToRomanNumbers;
