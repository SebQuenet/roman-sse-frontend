import {
  NumeralNotANumberError,
  NumeralTooLowError,
  NumeralTooHighError,
  NumeralNotAnIntegerError,
} from "./errors";
import { MINIMUM_ALLOWED_NUMERAL, MAXIMUM_ALLOWED_NUMERAL } from "./constants";

const formatNumeral = (value) => Number(value);

const validateNumeral = (numeralAsNumber) => {
  if (isNaN(numeralAsNumber)) {
    throw NumeralNotANumberError;
  }
  if (!Number.isInteger(numeralAsNumber)) {
    throw NumeralNotAnIntegerError;
  }
  if (numeralAsNumber < MINIMUM_ALLOWED_NUMERAL) {
    throw NumeralTooLowError;
  }
  if (numeralAsNumber > MAXIMUM_ALLOWED_NUMERAL) {
    throw NumeralTooHighError;
  }
};

const validateAndFormatNumeral = (numeral) => {
  const numeralAsNumber = formatNumeral(numeral);
  validateNumeral(numeralAsNumber);

  return numeralAsNumber;
};

export default validateAndFormatNumeral;
