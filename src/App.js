import React from "react";
import useNumeralToRomanNumber from "./useNumeralToRomanNumber";
import { handleNumeralChange } from "./handlers";
import styles from "./App.css";

const isConvertButtonDisabled = (numeral, isError) => numeral === "" || isError;

const App = () => {
  const {
    numeral,
    setNumeral,
    error,
    isError,
    handleSendButtonClick,
    romanNumber,
  } = useNumeralToRomanNumber();

  const sendButtonClick = (e) => {
    e.preventDefault();
    handleSendButtonClick();
  };

  try {
    return (
      <div>
        <h1>Numeral to roman number</h1>
        <form>
          <div>
            <label htmlFor="numeral">Numeral</label>
            <input
              className={styles.formInput}
              type="text"
              id="numeral"
              value={numeral}
              onChange={handleNumeralChange(setNumeral)}
            />
          </div>
          <p>{romanNumber}</p>
          <p>{error}</p>
          <button
            type="submit"
            disabled={isConvertButtonDisabled(numeral, isError)}
            onClick={sendButtonClick}
          >
            Convert to roman number
          </button>
        </form>
      </div>
    );
  } catch (error) {
    console.log(error);
    return <div>error</div>;
  }
};

export default App;
