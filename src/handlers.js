export const handleNumeralChange =
  (setNumeral) =>
  ({ target: { value } }) =>
    setNumeral(value);
